---?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Contexte]

@snap[north span-100]
![Header](assets/img/background.jpg)
@color[black](Préambule et contexte)
<br><br>
@fa[arrow-down text-orange]
@snapend

@snap[south span-100 text-05 text-grey]
Bio rapide | Historique VE 2.0 | Situation actuelle | Contraintes légales et projet
<br><br><br>
@snapend

Note:
- Fin: 1'30

+++?color=linear-gradient(to bottom, #dfdfe9, #ffffff)
@title[Bio]

@snap[north-east span-100 text-orange text-left text-12]
@fa[user]&nbsp;&nbsp;Bio rapide
@snapend

@snap[west text-black text-right text-05 span-22]
Christophe Vigouroux
<br><br>
*OCSIN, Etat de Genève*
<br><br><br><br><br><br>
@snapend

@snap[east span-70 text-08]
@ul[list-bullets-squares list-content-verbose]
* Architecte IT et lead developer sur projets vote électronique depuis 2010.
* Design, développement, documentation, opérations
* Expertise sécurité analyse et design
* Longue expérience secure coding, SSDLC
* Team développement protocole de vote
@ulend
@snapend

Note:
- Fin: 2'30

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Historique VE 2.0]
@snap[north span-100 text-orange text-left text-12]
@fa[scroll]&nbsp;&nbsp;Historique VE 2.0
@snapend

@snap[span-100 text-08 text-left text text-black]
<table>
<tr><td>@color[#FF5600](2013.12)</td><td>Nouvelle ordonnance fédérale sur le vote électronique</td></tr>
<tr><td>@color[#FF5600](2016.09)</td><td>Projet de loi pour financement programme VE 2.0</td></tr>
<tr><td>@color[#FF5600](2016.12)</td><td>Accord pour le lancement du programme</td></tr>
<tr><td>@color[#FF5600](2017<br>2018)</td><td>Programme VE 2.0: développement logiciels (réécriture complète), construction de l'infrastructure, démarrage du SMSI</td></tr>
<tr><td>@color[#FF5600](2018.11)</td><td>Décision du CE d'arrêter le développement du vote électronique</td></tr>
</table>
@snapend

Note:
- Fin: 3'30

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Situation actuelle]
@snap[north span-100 text-orange text-left text-12]
@fa[stopwatch]&nbsp;&nbsp;Situation actuelle
@snapend

@snap[span-100 text-08 text-left text text-black]
@ul[list-bullets-squares list-content-verbose]
* Projet arrêté, équipe et infrastructure décommisionnés
* Développements pour une votation terminés à 70%, pour une élection à 30%
* Possibilité d'exécuter une votation de bout-en-bout
* Publication sur Gitlab:  code source complet + documentation pour construire et exécuter le système
* Dernières spécifications du protocole non prises en compte par l'implémentation
@ulend
@snapend

Note:
- Fin: 4'30

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Contraintes légales et projet]
@snap[north span-100 text-orange text-left text-12]
@fa[balance-scale]&nbsp;&nbsp;Contraintes légales et projet
@snapend

@snap[span-100 text-08 text-left text text-black]
@ul[list-bullets-squares  list-content-concise]
* Réglementation fédérale graduelle
    * 30%, 50%, 100%
* Objectifs et contraintes Genève
    * Hébergement autres cantons et autonomie
    * Opensource
    * Budget déterminé avant spécification du protocole
    * Démarrer début 2017, finir avant fin 2018 pour certification début 2019
@ulend
@snapend

Note:
- Fin: 6'00
