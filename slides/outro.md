---?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Outro]

@snap[north span-100 text-black]
![Header](assets/img/background.jpg)
Merci de votre attention...
<br><br>
...et place aux question !
@snapend

@snap[south span-100 text-black text-06]
@fa[envelope]&nbsp;&nbsp;christophe.vigouroux@etat.ge.ch
@snapend

Note:
- Fin: 48'00