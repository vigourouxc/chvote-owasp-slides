---?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Sommaire]

@snap[north-west]
Sommaire
@snapend

@snap[south-west list-content-concise span-100]
@ol[](false)
* Préambule et contexte
* Modèle de sécurité
* Architecture générale de la solution
* Architecture distribuée du _protocol core_
* Modèle de menace du _protocol core_
* Perspectives
* Rétrospective
@olend
<br><br>
@snapend

Note:
- Fin: 1'00