---?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Modèle de sécurité]

@snap[north span-100]
![Header](assets/img/background.jpg)
@color[black](Modèle de sécurité)
<br><br>
@fa[arrow-down text-orange]
@snapend

@snap[south span-100 text-05 text-grey]
Objectifs de sécurité | Modèle de confiance | Limitations
<br><br><br>
@snapend

Note:
- Fin: 6'15

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Objectifs de sécurité]
@snap[north span-100 text-orange text-left text-12]
@fa[shield-alt]&nbsp;&nbsp;Objectifs de sécurité
@snapend

@snap[span-30 west text-08 text-center fragment]
@fa[person-booth text-orange]<br>Secret du vote<br><br><br><br><br><br><br>
@snapend

@snap[span-30 midpoint text-08 text-center fragment]
@fa[calculator text-orange]<br>Exactitude des résultats<br><br><br><br><br><br>
@snapend

@snap[span-30 east text-08 text-center fragment]
@fa[clock text-orange]<br>Pas de résultats anticipés<br><br><br><br><br><br>
@snapend

@snap[span-30 south-west text-08 text-center fragment]
@fa[burn text-orange]<br>Disponibilité<br><br><br><br><br>
@snapend

@snap[span-30 south text-08 text-center fragment]
@fa[user-shield text-orange]<br>Protection des informations personnelles<br><br><br>
@snapend

@snap[span-30 south-east text-08 text-center fragment]
@fa[user-tag text-orange]<br>Pas de preuves du comportement de vote<br><br><br>
@snapend

Note:
- Fin: 7'15

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Modèle de confiance]
@snap[north span-100 text-orange text-left text-12]
@fa[users]&nbsp;&nbsp;Modèle de confiance OVotE 100%
@snapend

@snap[midpoint span-70]
![Trust model](assets/img/trust-model.png)
@snapend

Note:
- Fin: 8'15

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Limitations]
@snap[north span-100 text-orange text-left text-12]
@fa[cloud-rain]&nbsp;&nbsp;Limitations
@snapend

@snap[span-50 west text-08 text-center fragment]
@fa[person-booth text-orange]<br>Sécurité du poste client
<br><br>
@ul[list-bullets-squares list-content-verbose](false)
* Secret du vote: acceptation politique et publique ?
* Manipulation du vote: participation active de l'utilisateur
* Initiative en cours (moratoire VE)
@ulend
@snapend

@snap[span-50 east text-08 text-center fragment]
@fa[user-check text-orange]<br>Authentification
<br><br>
@ul[list-bullets-squares list-content-verbose](false)
* De la carte de vote: PIN imprimé
* Du votant: date de naissance
@ulend
<br><br><br><br>
@snapend

Note:
- Fin: 9'45

