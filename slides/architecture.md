---?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Architecture générale CHVote 2.0]

@snap[north span-100]
![Header](assets/img/background.jpg)
@color[black](Architecture générale CHVote 2.0)
<br><br>
@fa[arrow-down text-orange]
@snapend

@snap[south span-100 text-05 text-grey]
Vue d'ensemble | Design du système | Choix d'implémentation
<br><br><br>
@snapend

Note:
- Fin: 10'00

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Vue d'ensemble]
@snap[north span-100 text-orange text-left text-12]
@fa[dot-circle]&nbsp;&nbsp;Vue d'ensemble
@snapend

@snap[south span-85]
![Vue d'ensemble](assets/img/architecture-simple.png)
@snapend

Note:
- Fin: 12'00

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Design du système - 1/3]
@snap[north span-100 text-orange text-left text-12]
@fa[drafting-compass]&nbsp;&nbsp;Design du système
@snapend

@snap[midpoint span-85]
![Architecture Online 1/3](assets/img/architecture-design-1.png)
@snapend

Note:
- Fin: 13'00

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Design du système - 2/3]
@snap[north span-100 text-orange text-left text-12]
@fa[drafting-compass]&nbsp;&nbsp;Design du système
@snapend

@snap[midpoint span-85]
![Architecture Online 2/3](assets/img/architecture-design-2.png)
@snapend

Note:
- Fin: 15'00

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Design du système - 3/3]
@snap[north span-100 text-orange text-left text-12]
@fa[drafting-compass]&nbsp;&nbsp;Design du système
@snapend

@snap[south span-85]
![Architecture Online 3/3](assets/img/architecture-design-3.png)
@snapend

Note:
- Fin: 16'30

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Choix d'architecture]
@snap[north span-100 text-orange text-left text-12]
@fa[crosshairs]&nbsp;&nbsp;Choix d'architecture
@snapend

@snap[west span-50 text-08 fragment]
@ul[list-bullets-squares list-content-concise text-06](false)
* **Docker, OpenShift/K8s**
    * Reproductibilité: dev/rec/prod, communauté
    * Testabilité
    * Haute dispo
    * *Contraintes:* construction nouvelle infra et exploitation, hardening
    * Pas pour les composants de contrôle (bare bone) !
@ulend
<br><br><br><br><br><br>
@snapend

@snap[east span-50 text-08 fragment]
@ul[list-bullets-squares list-content-concise text-06](false)
* **Message Broker RabbitMQ**
    * Protocole AMQP ouvert
    * Clients pour nombreux langages
    * Garantie de livraison des messages
    * Intégration Spring Boot
    * Fiabilité
    * *Contraintes:* petits messages seulement, clustering vs performances
    * *Décision:* Pas de clustering, découpage des gros messages
@ulend
@snapend

@snap[south-west span-50 text-08 fragment]
@ul[list-bullets-squares list-content-concise text-06](false)
* **Bases de données**
    * Standard OCSIN: Oracle RAC
    * *Contrainte:* coût des licences pour les composants de contrôle !
    * *Décision:* Composants de contrôle avec PostgreSQL, le reste Oracle RAC
    * Composants de contrôle testés avec succès avec MongoDB
@ulend
@snapend

Note:
- Fin: 18'30
