---?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Perspectives]

@snap[north span-100]
![Header](assets/img/background.jpg)
@color[black](Perspectives)
<br><br>
@fa[arrow-down text-orange]
@snapend

@snap[south span-100 text-05 text-grey]
Publication du projet sur Gitlab | Reprise du projet ? | Critères de succès
<br><br><br>
@snapend

Note:
- Fin: 40'45

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Publication du projet sur Gitlab]
@snap[north span-100 text-orange text-left text-12]
@fab[gitlab]&nbsp;&nbsp;Publication du projet sur Gitlab
@snapend

@snap[west span-48]
![Gitlab screenshot](assets/img/gitlab.png)
@snapend

@snap[east span-48 text-08 text-left text text-black]
@ul[list-bullets-squares list-content-verbose](false)
* https://chvote2.gitlab.io
* Demain mercredi 05/06/2019 sauf contre-ordre
* Code source complet, documentation fonctionnelle et technique, manuel développeur.
* Support limité :(
@ulend
@snapend

Note:
- Fin: 41'15

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Reprise du projet ?]
@snap[north span-100 text-orange text-left text-12]
@fa[fast-backward]&nbsp;&nbsp;Reprise du projet ?
@snapend

@snap[span-100 text-black text-left text-08]
@ul[list-bullets-squares list-content-verbose](false)
* Projet de loi GE pour un VE en mains publics (PL 12415A)
* Contexte différent
    * Règles d'agrément ChF vont changer
    * Vers plus d'ouverture du code
    * Moratoire VE
* Coûts de reprise non estimés
@ulend
@snapend

Note:
- Fin: 42'00

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Critères de succès]
@snap[north span-100 text-orange text-left text-12]
@fa[thumbs-up]&nbsp;&nbsp;Critères de succès
@snapend

@snap[span-100 north text-orange text-right text-05]
<br>
@fa[exclamation-triangle]&nbsp;&nbsp;Avis personnel
@snapend

@snap[west span-48]
<br><br>
@box[text-black text-06 span-100 fragment](Exploitation indépendante des composants de contrôle # Organisations indépendantes: financièrement, politiquement, hiérarchiquement, technologiquement.)
@box[text-black text-06 span-100 fragment](Implémentations indépendantes des composants de contrôle # Langages différents: java/go/ruby/rust, OS différents, Hardwares différents, Persistances différentes: PostgreSQL/MongoDB, etc.)
@box[text-black text-06 span-100 fragment](Organisations spécifiques # Réelle difficulté à exploiter un système aussi spécifique dans le cadre des processus standards et des ressources mutualisées d'une grande organisation)
@snapend

@snap[east span-48]
<br><br>
@box[text-black text-06 span-100 fragment](Plannification # Ne pas se donner comme objectif absolu l'utilisation pour une votation ou élection en particulier. Petite équipe focus sur le sujet.)
@box[text-black text-06 span-100 fragment](Partenariats # Continuer voire accroître les partenariats académiques, code source ouvert en tout temps et prise en compte retours communauté crypto.)
@box[text-black text-06 span-100 fragment](Identité numérique # Pour ne plus dépendre de la date de naissance et authentifier plus que juste la carte de vote.)
@box[text-black text-06 span-100 fragment](Accepter certaines limitations # S'assurer de l'acceptation publique des limites du secret du vote, alternative = vote par codes)
@snapend

Note:
- Fin: 45'00
