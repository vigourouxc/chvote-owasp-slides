---?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Rétrospective]

@snap[north span-100]
![Header](assets/img/background.jpg)
@color[black](Rétrospective)
<br><br>
@fa[arrow-down text-orange]
@snapend

@snap[south span-100 text-05 text-grey]
Speedboat | Améliorations SSLDC
<br><br><br>
@snapend

Note:
- Fin: 45'15

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Speedboat]
@snap[north span-100 text-orange text-left text-12]
@fa[ship]&nbsp;&nbsp;Speedboat
@snapend

@snap[south span-80 text-03]
@img[](assets/img/retro-speedboat.png)<br>
Source du Speedboat: https://www.ux-republic.com/ux-calendar-12-decembre-speedboat/
@snapend

Note:
- Fin: 47'15

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Améliorations SSLDC]
@snap[north span-100 text-orange text-left text-12]
@fa[fingerprint]&nbsp;&nbsp;Améliorations SSDLC
@snapend

@box[text-black text-08 span-100 fragment](Gestion des vulnérabilités # Ajouter une étape dans le CI pour scans vulnérabilités Docker, Maven, Javascript)
<br>
@box[text-black text-08 span-100 fragment](Tests de sécurité # Inclure description des abuse-cases et développement tests de sécurité automatisés dans DoD)

Note:
- Fin: 47'45
