---?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Architecture distribuée protocol core]

@snap[north span-100]
![Header](assets/img/background.jpg)
@color[black](Architecture distribuée du *protocol core*)
<br><br>
@fa[arrow-down text-orange]
@snapend

@snap[south span-100 text-05 text-grey]
Introduction | Principe des composants de contrôle | Contraintes | Propriétés  | Directives d'implémentation
<br><br><br>
@snapend

Note:
- Fin: 18'45

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Introduction]
@snap[north span-100 text-orange text-left text-12]
@fa[feather-alt]&nbsp;&nbsp;Introduction
@snapend
@quote[My First Law of Distributed Object Design: Don't distribute your objects.](Martin Fowler)

Note:
- Fin: 19'15

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Principe soumission du bulletin]
@snap[north span-100 text-orange text-left text-12]
@fa[archive]&nbsp;&nbsp;Principe soumission du bulletin
@snapend
@snap[south span-55]
![Soumission](assets/img/cc-submission.png)
@snapend

Note:
- Fin: 21'15

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Principe déchiffrement]
@snap[north span-100 text-orange text-left text-12]
@fa[archive]&nbsp;&nbsp;Principe déchiffrement
@snapend
@snap[south span-85]
![Déchiffrement](assets/img/cc-decrypt-v3.png)
@snapend

Note:
- Fin: 23'45

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Contraintes architecturales]
@snap[north span-100 text-orange text-left text-12]
@fa[hard-hat]&nbsp;&nbsp;Contraintes architecturales
@snapend

@snap[west span-48]
@box[text-black text-08 span-100 fragment](Architecture distribuée # Architecture par nature distribuée: fondement de la sécurité apportée par les composants de contrôles.)

@box[text-black text-08 span-100 fragment](Niveaux de service élevés # Haute dispo, pas de perte de données, cohérence des données, haute performance.)
@snapend

@snap[east span-48]
@box[text-black text-08 span-100 fragment](Flexibilité # Scaling horizontal et vertical pour acceuillir de nouveaux cantons.<br><br>)

@box[text-black text-08 span-100 fragment](Temps de traitement élevés # Capacité de traiter des demandes pouvant prendre quelques minutes à plusieurs heures.)
@snapend

Note:
- Fin: 25'15

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Propriétés]
@snap[north span-100 text-orange text-left text-12]
@fa[ruler-combined]&nbsp;&nbsp;Propriétés architecturales
@snapend

@snap[west span-48]
@box[text-black text-08 span-100 fragment](Résistance aux pannes # )
@box[text-black text-08 span-100 fragment](*Eventual consistency* # )
@box[text-black text-08 span-100 fragment](Elasticité # )
@snapend

@snap[east span-48]
@box[text-black text-08 span-100 fragment](Gestion des traitements longs # )
@box[text-black text-08 span-100 fragment](Couplage lâche # )
@box[text-black text-08 span-100 fragment](=> Event driven architecture # )
@snapend

Note:
- Fin: 27'15

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Directives d'implémentation]
@snap[north span-100 text-orange text-left text-12]
@fa[pen-nib]&nbsp;&nbsp;Directives d'implémentation
@snapend

@snap[south-west span-48]
@box[text-black text-08 span-100 fragment](At-least-once delivery # Garantie *soft* par le middleware, assuré par l'applicatif)
@box[text-black text-08 span-100 fragment](Retries émetteur/récepteur # Gestion des pannes et des erreurs recouvrables<br>Mécanisme de ACK)
@box[text-black text-08 span-100 fragment](Idempotence # Service consommateur DOIT être idempotent - déduplication ou pas de changement état interne)
@snapend

@snap[south-east span-48]
@box[text-black text-08 span-100 fragment](Messages texte/JSON # Interopérabilité, performances, sécurité)
@box[text-black text-08 span-100 fragment](Messages load-balancés # Consommateurs déployés sous forme de pool d'instances, une seule doit traiter le message)
@box[text-black text-08 span-100 fragment](Pas de XA/2PC # Interopérabilité<br>Testabilité<br>Simplicité)
@snapend

Note:
- Fin: 30'15