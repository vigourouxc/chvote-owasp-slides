---?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Modèle de menace protocol core]

@snap[north span-100]
![Header](assets/img/background.jpg)
@color[black](Modèle de menace du *protocol core*)
<br><br>
@fa[arrow-down text-orange]
@snapend

@snap[south span-100 text-05 text-grey]
Méthodologie | Dépendances externes | Points d'entrée | Niveaux d'accès | Assets | Diagramme des flux de données | Approche de détermination | Matrice des risques
<br><br><br>
@snapend

Note:
- Fin: 30'30

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Méthodologie]
@snap[north span-100 text-orange text-left text-12]
@fa[book]&nbsp;&nbsp;Méthodologie
@snapend

@snap[span-100 text-05 text-left text-black]
Basé sur ancienne version publiée par OWASP: https://www.owasp.org/index.php?title=Application_Threat_Modeling&oldid=191010
<br><br>
@snapend

@snap[span-100 text-06 text-left text-black list-content-concise]
@ol[](false)
* Définir le périmètre du modèle
* Lister les dépendences externes
* Déterminer les points d'entrée
* Déterminer les niveaux d'accès
* Lister les bien essentiels et de support (assets)
* Schématiser le diagramme de flux des données
* Choisir une approche pour l'identification des menaces
* Déterminer les menaces
* Déterminer leurs contre-mesures
* Evaluer les risques
@olend
@snapend

@snap[east span-30 text-05 text-orange]
@fa[sync-alt fa-10x]
@snapend

Note:
- Fin: 31'30

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Dépendances externes]
@snap[north span-100 text-orange text-left text-12]
@fa[tools]&nbsp;&nbsp;Dépendances externes
@snapend

@snap[west span-30]
@box[text-black text-08 span-100 fragment](Serveur composant de contrôle # )
@box[text-black text-08 span-100 fragment](Message broker # )
@box[text-black text-08 span-100 fragment](Centralisation des logs # )
@snapend

@snap[midpoint span-30]
@box[text-black text-08 span-100 fragment](Monitoring système # )
@box[text-black text-08 span-100 fragment](Validation des certificats # )
@box[text-black text-08 span-100 fragment](Contrôles d'accès physique # )
@snapend

@snap[east span-30]
@box[text-black text-08 span-100 fragment](Storage Area Network # )
@box[text-black text-08 span-100 fragment](Réseau inter-datacenter # )
@snapend

Note:
- Fin: 32'15

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Points d'entrée]
@snap[north span-100 text-orange text-left text-12]
@fa[door-open]&nbsp;&nbsp;Points d'entrée
@snapend

@img[span-90](assets/img/cc-entry-points-v2.png)

Note:
- Fin: 33'00

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Niveaux d'accès]
@snap[north span-100 text-orange text-left text-12]
@fa[user-circle]&nbsp;&nbsp;Niveaux d'accès
@snapend

@img[span-90](assets/img/cc-trust-levels.png)

Note:
- Fin: 33'30

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Biens essentiels]
@snap[north span-100 text-orange text-left text-12]
@fa[money-bill-alt]&nbsp;&nbsp;Biens essentiels
@snapend

@snap[span-100 text-black text-left text-08]
@css[text-orange](Définition:) @fa[quote-left]&nbsp;&nbsp;*Information ou processus jugé comme important pour l'organisme. On appréciera ses besoins de sécurité mais pas ses vulnérabilités.*<br><br>
@css[text-orange](Exemples:)<br><br>
@ul[list-bullets-squares list-content-concise text-06](false)
* Bulletin de vote
* Electeur
* Clé publique de chiffrement
* Clé privée de chiffrement des bulletins d'un composant de contrôle
* Clé privée du certificat d'un composant de contrôle
@ulend
@snapend

@snap[south span-100 text-black text-left text-05]
*Définition selon EBIOS (méthodologie d'analyse des risques)*
@snapend

Note:
- Fin: 34'30

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Biens support]
@snap[north span-100 text-orange text-left text-12]
@fa[server]&nbsp;&nbsp;Biens support
@snapend

@snap[span-100 text-black text-left text-08]
@css[text-orange](Définition:) @fa[quote-left]&nbsp;&nbsp;*Bien sur lequel reposent des biens essentiels. On distingue notamment les systèmes informatiques, les organisations et les locaux. On appréciera ses vulnérabilités mais pas ses besoins de sécurité.*<br><br>
@css[text-orange](Exemples:)<br><br>
@ul[list-bullets-squares list-content-concise text-06](false)
* Binaires de l'application
* Fichiers de log
* Base de données
* OS et serveur
* Abilité à accéder aux composants de contrôle
@ulend
@snapend

@snap[south span-100 text-black text-left text-05]
*Définition selon EBIOS (méthodologie d'analyse des risques)*
@snapend

Note:
- Fin: 35'30

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Diagramme flux de données]
@snap[north span-100 text-orange text-left text-12]
@fa[project-diagram]&nbsp;&nbsp;Diagramme de flux de données
@snapend

@img[span-100](assets/img/cc-dfd.png)

Note:
- Fin: 37'30

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Approche détermination des menaces]
@snap[north span-100 text-orange text-left text-12]
@fa[user-secret]&nbsp;&nbsp;Approche de détermination des menaces
@snapend

@snap[north span-100 text-center text-black]
<br><br>
S.T.R.I.D.E.
@snapend

@snap[west span-48]
<br><br>
@box[text-black text-08 span-100 fragment](Spoofing # )
@box[text-black text-08 span-100 fragment](Tampering # )
@box[text-black text-08 span-100 fragment](Repudiation # )
@snapend

@snap[east span-48]
<br><br>
@box[text-black text-08 span-100 fragment](Information Disclosure # )
@box[text-black text-08 span-100 fragment](Denial of Service # )
@box[text-black text-08 span-100 fragment](Elevation of Privilege # )
@snapend

Note:
- Fin: 39'00

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Matrice des risques 1/2]
<br>
@snap[north span-100 text-orange text-left text-12]
@fa[radiation-alt]&nbsp;&nbsp;Matrice des risques 1/2
@snapend
@snap[midpoint span-70]
<br>
@img[](assets/img/cc-threats-1.png)
@snapend

Note:
- Fin: 40'00

+++?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Matrice des risques 2/2]
@snap[north span-100 text-orange text-left text-12]
@fa[radiation-alt]&nbsp;&nbsp;Matrice des risques 2/2
@snapend
@snapend
@snap[midpoint span-70]
<br>
@img[](assets/img/cc-threats-2.png)
@snapend

Note:
- Fin: 40'30