---?color=linear-gradient(to right, #dfdfe9, #ffffff)
@title[Introduction]

@snap[west text-15 text-bold text-black]
CHVote 2.0

![Header](assets/img/header.jpg)

Modèle de sécurité<br>
Protocole et architecture distribués<br>
@snapend

---?include=slides/summary.md
---?include=slides/context.md
---?include=slides/security-model.md
---?include=slides/architecture.md
---?include=slides/distributed-architecture.md
---?include=slides/threat-model.md
---?include=slides/whats-next.md
---?include=slides/retrospective.md
---?include=slides/outro.md