[![GitPitch](https://gitpitch.com/assets/badge.svg)](https://gitpitch.com/vigourouxc/chvote-owasp-slides/master?grs=gitlab)

# CHVote presentation of the architecture and of the security model 

This presentation is currently only available in french. Intended to be used for
the OWASP meeting on 4. June 2019. Designed to be reused later for explaining the
architectural and security concepts of the CHVote product.